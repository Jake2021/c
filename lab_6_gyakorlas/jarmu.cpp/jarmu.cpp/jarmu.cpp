#include <iostream>
/// Utas�t�st ki�r� �s v�grehajt� makr�
#define _(x)  std::cout << #x << std::endl; x


class Jarmu {
	double vMax;
public:
	Jarmu() {};
	Jarmu(double v) :vMax(v) { std::cout << "\tJarmu ctor vMax=" << vMax << std::endl; };
	~Jarmu() { std::cout << "\tJarmu dtor vMax=" << vMax << std::endl; };
	Jarmu(const Jarmu& k) :vMax(k.vMax) { std::cout << "\tJarmu copy vMax=" << vMax << std::endl; };
};

class Szan :public Jarmu {
	int kutyakSzama;
public:
	Szan(double v = 0, int n = 0) :Jarmu(v), kutyakSzama(n) { std::cout << "\tSzan ctor kutyakSzama=" << kutyakSzama << std::endl; };
	~Szan() { std::cout << "\tSzan dtor kutyakSzama=" << kutyakSzama << std::endl; };	
	Szan(const Szan& s) :kutyakSzama(s.kutyakSzama) { std::cout << "\tSzan copy kutyakSzama=" << kutyakSzama << std::endl; };
};


int main() {
	_(Szan sz0_obj; )
		_(Szan sz1_obj(1.1, 1); )
		_(Szan sz1m_obj = sz1_obj; )
		_(return 0; )
}