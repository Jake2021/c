
#include <iostream>
/// Utas�t�st ki�r� �s v�grehajt� makr�
#define _(x)  std::cout << #x << std::endl; x


class Kerek {
	int atmero;
public:
	Kerek(const int x) 
	{ 
		this->atmero = x;
		std::cout << "\tKerek ctor\n";
	};
	~Kerek() { std::cout << "\tKerek dtor\n"; };
	void kiir() { std::cout << "atmero=" << atmero << std::endl; };
	Kerek(const Kerek& k) :atmero(k.atmero)//:atmero(k.atmero) itt van az mikor atadja az �rt�k�t vagyis felt�lt�m �rt�kkel
	{
		std::cout << "\tKerek copy\n";
	}
};

class Monocikli {
	Kerek k;
public:
	Monocikli() :k(16) {std::cout << "\tMonocikli ctor\n";	};
	//~Monocikli() { std::cout << "\tMonocikli dtor\n"; };
	void kiir() { std::cout << "\tk."; k.kiir(); };
	Monocikli(const Monocikli& k) :k(16) { std::cout << "\tMonocikli copy\n"; }; //Ha van a Monocikli oszt�lynak is m�sol� konstruktora akkor az fog megh�v�dni nem pedig a parent class m�sol� konstruktora
};

//Mi�rt kell mindk�t helyre a :k(16) ? a m�sol� konstruktorba �s a sima l�trehoz�ba is �gy nem dupl�n inicializ�lom?




int main() {
	_(Monocikli m1_obj; )
		_(m1_obj.kiir(); )
		_(Monocikli m2_obj = m1_obj; )
		_(m2_obj.kiir(); )
		_(return 0; )
}